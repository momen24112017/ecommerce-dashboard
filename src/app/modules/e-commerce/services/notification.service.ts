import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  url = "http://marouf.leatherworks.giraffecode.com/public/api";

  constructor(private http: HttpClient) { }
  sendNotificationToall(heading, contents) {
    return this.http.post(`${this.url}/sendNotification`, {
      "headings": heading,
      "contents": contents
    })
  }
  sendNotificationtouser(user_id,heading, contents){
    return this.http.post(`${this.url}/sendNotificationtoUser`, {
      "user_id":user_id,
      "headings": heading,
      "contents": contents
    })
  }


}

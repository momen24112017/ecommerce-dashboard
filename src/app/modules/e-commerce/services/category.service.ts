import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  url = "http://marouf.leatherworks.giraffecode.com/public/api";
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'multipart/form-data; boundary=something',
      'host': '<calculated when request is sent>'
    })
  };
  constructor(private http: HttpClient) { }
  listCategories() {
    return this.http.get(`${this.url}/listCategory`);
  }
  viewCat(id) {
    return this.http.get(`${this.url}/editCategory/${id}`)
  }
  deleteCat(id) {
    return this.http.get(`${this.url}/deleteCategory/${id}`)
  }
  bulkDelete(arr) {
    return this.http.post(`${this.url}/bulkDeleteCategory`, arr)
  }
  editCat(id, obj) {
    console.log('ooo', obj);
    return this.http.post(`${this.url}/updateCategory/${id}`, obj)
  }
  updateCategoryImage(id, file) {
    let options;
    options = {headers:this.httpOptions}
    return this.http.post(`${this.url}/updateCategory/${id}`, file)
  }
  addCat(obj) {
    return this.http.post(`${this.url}/createCategory`, obj)
  }
  editCatimage() {

  }
}

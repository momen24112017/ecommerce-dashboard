import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  url = "http://marouf.leatherworks.giraffecode.com/public/api"
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'multipart/form-data; boundary=something',
      'Content-Length':'<calculated when request is sent>',
      'host': '<calculated when request is sent>'
    })
  };
  constructor(private http: HttpClient) { }
  getproducts() {
    return this.http.get(`${this.url}/getProducts`)
  }
  getproduct(id) {
    return this.http.get(`${this.url}/productDetails/${id}`)
  }
  createProduct(obj) {
    return this.http.post(`${this.url}/createProduct`, obj)
  }
  editProduct(obj, id) {
    return this.http.post(`${this.url}/updateProduct/${id}`, obj)
  }
  editImages(file,id){
    return this.http.post(`${this.url}/productDetails/${id}`,file)
  }
  deleteProduct(id) {
    return this.http.get(`${this.url}/deleteProduct/${id}`)
  }
  bulkDeleteProducts(obj) {
    return this.http.post(`${this.url}/bulkDeleteProduct`, obj)
  }

  getusers(){
    return this.http.get(`${this.url}/listUsers`)
  }

  getOrders(){
    return this.http.get(`${this.url}/listOrders`)

  }

}

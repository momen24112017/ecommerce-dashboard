import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { textChangeRangeIsUnchanged } from 'typescript';

@Injectable({
  providedIn: 'root'
})
export class DeliveryFeesService {
  url = "http://marouf.leatherworks.giraffecode.com/public/api";
  constructor(private http: HttpClient) { }
  getDeliveryFees() {
    return this.http.get(`${this.url}/listDeliveryFees`)
  }
  deleteFee(id){
    return this.http.get(`${this.url}/deleteDeliveryFees/${id}`)
  }
  getDeliveryFee(id){
    return this.http.get(`${this.url}/editDeliveryFees/${id}`)
  }
  updateFee(obj,id){
    return this.http.post(`${this.url}/updateDeliveryFees/${id}`,obj);
  }
  newFee(obj){
    return this.http.post(`${this.url}/createDeliveryFees`,obj)
  }
  bulkDelete(ids){
    return this.http.post(`${this.url}/bulkDeleteDeliveryFees`,ids)
  }
}

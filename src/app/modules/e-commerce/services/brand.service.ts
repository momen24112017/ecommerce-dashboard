import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BrandSettingService {
  url = "http://marouf.leatherworks.giraffecode.com/public/api";

  constructor(private http: HttpClient) { }
  getBrandSetting() {
    return this.http.get(`${this.url}/getBrandSetting`);
  }
}

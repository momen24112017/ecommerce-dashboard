// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-delete-category-modal',
//   templateUrl: './delete-category-modal.component.html',
//   styleUrls: ['./delete-category-modal.component.scss']
// })
// export class DeleteCategoryModalComponent implements OnInit {

//   constructor() { }

//   ngOnInit(): void {
//   }

// }
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of, Subscription } from 'rxjs';
import { catchError, delay, finalize, tap } from 'rxjs/operators';
import { CategoryService } from '../../services/category.service';
import { CustomersService } from '../../_services';

@Component({
  selector: 'app-delete-category-modal',
  templateUrl: './delete-category-modal.component.html',
  styleUrls: ['./delete-category-modal.component.scss']
})
export class DeleteCategoryModalComponent implements OnInit, OnDestroy {
  @Input() id: number;
  isLoading = false;
  subscriptions: Subscription[] = [];

  constructor(private catService: CategoryService,private customersService: CustomersService, public modal: NgbActiveModal) { }

  ngOnInit(): void {
  }

  deleteCustomer() {
    this.isLoading = true;
    this.catService.deleteCat(this.id).subscribe(res => {
      this.isLoading = true;
      this.modal.close()
    }, err => {
      this.isLoading = true;
      this.modal.close()
    })
    // const sb = this.customersService.delete(this.id).pipe(
    //   delay(1000), // Remove it from your code (just for showing loading)
    //   tap(() => this.modal.close()),
    //   catchError((err) => {
    //     this.modal.dismiss(err);
    //     return of(undefined);
    //   }),
    //   finalize(() => {
    //     this.isLoading = false;
    //   })
    // ).subscribe();
    // this.subscriptions.push(sb);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sb => sb.unsubscribe());
  }
}

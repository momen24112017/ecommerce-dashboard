// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-delete-categories-modal',
//   templateUrl: './delete-categories-modal.component.html',
//   styleUrls: ['./delete-categories-modal.component.scss']
// })
// export class DeleteCategoriesModalComponent implements OnInit {

//   constructor() { }

//   ngOnInit(): void {
//   }

// }
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of, Subscription } from 'rxjs';
import { catchError, delay, finalize, tap } from 'rxjs/operators';
import { CategoryService } from '../../services/category.service';
import { CustomersService } from '../../_services';

@Component({
  selector: 'app-delete-categories-modal',
  templateUrl: './delete-categories-modal.component.html',
  styleUrls: ['./delete-categories-modal.component.scss']
})
export class DeleteCategoriesModalComponent implements OnInit, OnDestroy {
  @Input() ids: number[];
  isLoading = false;
  subscriptions: Subscription[] = [];

  constructor(private catService:CategoryService,private customersService: CustomersService, public modal: NgbActiveModal) { }

  ngOnInit(): void {
  }

  deleteCustomers() {
    this.isLoading = true;
    this.catService.bulkDelete(this.ids).subscribe(res=>{
      console.log(res);
      this.modal.close()
    },err=>{
      console.log(err);
      this.modal.close()
    })
    // const sb = this.customersService.deleteItems(this.ids).pipe(
    //   delay(1000), // Remove it from your code (just for showing loading)
    //   tap(() => this.modal.close()),
    //   catchError((errorMessage) => {
    //     this.modal.dismiss(errorMessage);
    //     return of(undefined);
    //   }),
    //   finalize(() => {
    //     this.isLoading = false;
    //   })
    // ).subscribe();
    // this.subscriptions.push(sb);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sb => sb.unsubscribe());
  }
}

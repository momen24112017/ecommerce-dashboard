import { ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable, of, Subscription } from 'rxjs';
import { catchError, switchMap, tap } from 'rxjs/operators';
import { CategoryService } from '../../services/category.service';
import { Product } from '../../_models/product.model';
import { ProductsService } from '../../_services';

const EMPTY_PRODUCT: Product = {
  id: undefined,
  model: '',
  manufacture: 'Pontiac',
  modelYear: 2020,
  mileage: 0,
  description: '',
  color: 'Red',
  price: 0,
  condition: 1,
  status: 1,
  VINCode: '',
};
@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})

export class EditCategoryComponent implements OnInit, OnDestroy {
  id: number;
  promocodeObj = {
    id: '1',
    promocode: 'ahmed',
    value: 'naeg',
    valid_date: '20-11-2020'

  };
  catObj = {
    id: '',
    name: '',
    image: 'assets/media/bg/dummy-post-horisontal.jpg'
  }
  previous: Product;
  formGroup: FormGroup;
  isLoading: boolean;
  errorMessage = '';
  tabs = {
    BASIC_TAB: 0,
    REMARKS_TAB: 1,
    SPECIFICATIONS_TAB: 2
  };
  dummy;
  activeTabId = this.tabs.BASIC_TAB; // 0 => Basic info | 1 => Remarks | 2 => Specifications
  private subscriptions: Subscription[] = [];
  page_type;
  imageChanged: boolean = false;
  @ViewChild('savebutton') savebutton: ElementRef;
  constructor(
    private fb: FormBuilder,
    private productsService: ProductsService,
    private router: Router,
    private route: ActivatedRoute,
    private catService: CategoryService,
    private cd: ChangeDetectorRef,
    private toast: ToastrService
  ) { }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.get('id')) {
      this.loadProduct();
      console.log('this is edit page');
      this.page_type = 'edit';
    } else {
      console.log('this is add page');
      this.page_type = 'add';
      this.formGroup = this.fb.group({
        name: [this.catObj.name, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(100)])],
      });
    }
  }
  loadProduct() {
    this.isLoading = true;
    const sb = this.route.paramMap.pipe(
      switchMap(params => {
        // get id from URL
        this.id = Number(params.get('id'));
        if (this.id || this.id > 0) {
          return this.productsService.getItemById(this.id);
        }
        return of(EMPTY_PRODUCT);
      }),
      catchError((errorMessage) => {
        this.errorMessage = errorMessage;
        return of(undefined);
      }),
    ).subscribe((res: Product) => {
      // if (!res) {
      //   this.router.navigate(['/products'], { relativeTo: this.route });
      // }

      this.catService.viewCat(this.id).subscribe(res => {
        let data;
        data = res
        this.catObj = data.data[0];
        console.log('this is cat obj', this.catObj);
        this.isLoading = false;

        this.loadForm();
      }, err => {
        this.isLoading = false;
        this.toast.error('Something went wrong')

      });
      this.previous = Object.assign({}, res);
    });
    this.subscriptions.push(sb);
  }

  loadForm() {
    if (!this.promocodeObj) {
      return;
    }
    this.formGroup = this.fb.group({
      name: [this.catObj.name, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(100)])],
    });
    this.cd.detectChanges();
  }

  reset() {
    if (!this.previous) {
      return;
    }

    // this.promocodeObj = Object.assign({}, this.previous);
    this.formGroup.reset();
  }

  save() {
    this.savebutton.nativeElement.disabled = true;
    this.isLoading = true;      
    if (this.page_type == 'edit') {
      console.log(this.formGroup.value);
      this.catService.editCat(this.id, this.formGroup.value).subscribe(res => {
        console.log(res);

        //update image
        if (this.imageChanged == false) {
          this.savebutton.nativeElement.disabled = false;
          this.isLoading = false;
        } else if (this.imageChanged == true) {
          console.log('will change image');
          var formdata = new FormData();
          formdata.append('image',this.imgfile)
          this.catService.updateCategoryImage(this.id, formdata).subscribe(res => {
            console.log('image response',res);
            this.savebutton.nativeElement.disabled = false;
            this.isLoading = false;
          }, err => {
            console.log('image error',err);
            this.savebutton.nativeElement.disabled = false;
            this.isLoading = false;
          })
        }
      }, err => {
        this.isLoading = false;
        this.savebutton.nativeElement.disabled = false;
        console.log(err);
      })

    } else if (this.page_type == 'add') {
      if (this.catObj.image == 'assets/media/bg/dummy-post-horisontal.jpg') {
        this.toast.error('Please select category image');
        this.isLoading = false;
        this.savebutton.nativeElement.disabled = false;

      } else {
        this.catService.addCat(this.formGroup.value).subscribe(res => {
          console.log(res);
          this.savebutton.nativeElement.disabled = false;
          this.formGroup.reset();
          this.isLoading = false;
          this.router.navigate(['/ecommerce/delivery-fees'])
        }, err => {
          this.savebutton.nativeElement.disabled = false;
          console.log(err);
          this.isLoading = false;

        })

      }
    }
  }
  selectedFile: File;
  imgfile;
  imagesbase64
  getImage(file,id) {
    this.imageChanged = true;
    console.log(file);
    this.imgfile = file[0]
    var reader = new FileReader();
    reader.readAsDataURL(file[0]);
    reader.onload = ()=>{
      (<HTMLElement>document.querySelector(`#${id}`)).setAttribute('src', <string>reader.result);      
    }

  
  }

  edit() {
    const sbUpdate = this.productsService.update(this.promocodeObj).pipe(
      tap(() => this.router.navigate(['/ecommerce/products'])),
      catchError((errorMessage) => {
        console.error('UPDATE ERROR', errorMessage);
        return of(this.promocodeObj);
      })
    ).subscribe(res => this.promocodeObj = res);
    this.subscriptions.push(sbUpdate);
  }

  create() {
    const sbCreate = this.productsService.create(this.promocodeObj).pipe(
      tap(() => this.router.navigate(['/ecommerce/products'])),
      catchError((errorMessage) => {
        console.error('UPDATE ERROR', errorMessage);
        return of(this.promocodeObj);
      })
    ).subscribe(res => {
      // this.promocodeObj = res as Product
    }



    );
    this.subscriptions.push(sbCreate);
  }

  changeTab(tabId: number) {
    this.activeTabId = tabId;
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sb => sb.unsubscribe());
  }

  // helpers for View
  isControlValid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation: string, controlName: string) {
    const control = this.formGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.dirty || control.touched;
  }
  back() {
    this.router.navigate(['/ecommerce/promo-code'])
  }
}

import { ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable, of, Subscription } from 'rxjs';
import { catchError, switchMap, tap } from 'rxjs/operators';
import { DeliveryFeesService } from '../../services/delivery-fees.service';
import { ProductService } from '../../services/product.service';
import { Product } from '../../_models/product.model';
import { ProductsService } from '../../_services';

const EMPTY_PRODUCT: Product = {
  id: undefined,
  model: '',
  manufacture: 'Pontiac',
  modelYear: 2020,
  mileage: 0,
  description: '',
  color: 'Red',
  price: 0,
  condition: 1,
  status: 1,
  VINCode: '',
};

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})
export class ProductEditComponent implements OnInit, OnDestroy {
  id: number;
  promocodeObj = {
    id: '1',
    promocode: 'ahmed',
    value: 'naeg',
    valid_date: '20-11-2020'

  };
  resObj = {
    id: '',
    name: '',
    price: '',
    fake_quantity: '',
    long_desc: '',
    short_desc: '',
    is_feature: '',
    quantity:'',
    images: '',
    second_image: '',
    third_image: ''
  }
  previous: Product;
  formGroup: FormGroup;
  isLoading: boolean;
  errorMessage = '';
  tabs = {
    BASIC_TAB: 0,
    REMARKS_TAB: 1,
    SPECIFICATIONS_TAB: 2
  };
  activeTabId = this.tabs.BASIC_TAB; // 0 => Basic info | 1 => Remarks | 2 => Specifications
  private subscriptions: Subscription[] = [];
  page_type;
  @ViewChild('savebutton') savebutton: ElementRef;
  constructor(
    private fb: FormBuilder,
    private productsService: ProductsService,
    private router: Router,
    private route: ActivatedRoute,
    private productService: ProductService,
    private cd: ChangeDetectorRef,
    private producService: ProductService,
    private toast:ToastrService
  ) { }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.get('id')) {
      this.loadProduct();
      console.log('this is edit page');
      this.page_type = 'edit';
    } else {
      console.log('this is add page');
      this.page_type = 'add';
      this.formGroup = this.fb.group({
        name: [this.resObj.name, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(100)])],
        price: [this.resObj.price, Validators.compose([Validators.required,])],
        fake_quantity: [this.resObj.fake_quantity, Validators.compose([Validators.required,])],
        long_desc: [this.resObj.long_desc, Validators.compose([Validators.required,])],
        short_desc: [this.resObj.short_desc, Validators.compose([Validators.required,])],
        is_feature: [this.resObj.is_feature, Validators.compose([Validators.required,])]
      });
    }
  }
  loadP
  loadProduct() {
    const sb = this.route.paramMap.pipe(
      switchMap(params => {
        // get id from URL
        this.id = Number(params.get('id'));
        if (this.id || this.id > 0) {
          return this.productsService.getItemById(this.id);
        }
        return of(EMPTY_PRODUCT);
      }),
      catchError((errorMessage) => {
        this.errorMessage = errorMessage;
        return of(undefined);
      }),
    ).subscribe((res: Product) => {
      // if (!res) {
      //   this.router.navigate(['/products'], { relativeTo: this.route });
      // }

      this.producService.getproduct(this.id).subscribe(res => {
        let data;
        data = res
        this.resObj = data.data.product_details[0];
        console.log(this.resObj);
        this.loadForm();
      });
      this.previous = Object.assign({}, res);
    });
    this.subscriptions.push(sb);
  }

  loadForm() {
    if (!this.promocodeObj) {
      return;
    }
    this.formGroup = this.fb.group({
      name: [this.resObj.name, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(100)])],
      price: [this.resObj.price, Validators.compose([Validators.required,])],
      fake_quantity: [this.resObj.fake_quantity, Validators.compose([Validators.required,])],
      long_desc: [this.resObj.long_desc, Validators.compose([Validators.required,])],
      short_desc: [this.resObj.short_desc, Validators.compose([Validators.required,])],
      is_feature: [this.resObj.is_feature, Validators.compose([Validators.required,])]

    });
    this.cd.detectChanges();
  }

  reset() {
    if (!this.previous) {
      return;
    }

    // this.promocodeObj = Object.assign({}, this.previous);
    this.formGroup.reset();
  }

  formData = new FormData();
  save() {
    this.savebutton.nativeElement.disabled = true;
    this.isLoading = true;
    if (this.page_type == 'edit') {
      console.log('this',this.formGroup.value);
      
      this.formData.append('name',this.formGroup.value.name)
      this.formData.append('fake_quantity',this.formGroup.value.fake_quantity)
      this.formData.append('long_desc',this.formGroup.value.long_desc)
      this.formData.append('short_desc',this.formGroup.value.short_desc)
      this.formData.append('price',this.formGroup.value.price)
      this.formData.append('is_feature',this.formGroup.value.is_feature)
      this.formData.append('quantity',this.formGroup.value.fake_quantity)

      this.producService.editProduct(this.formData, this.id).subscribe(res => {
        console.log(res);
        this.savebutton.nativeElement.disabled = false;
        this.isLoading = false;
        this.toast.success('product updated successfully')
      }, err => {
        this.isLoading = false;
        this.savebutton.nativeElement.disabled = false;
        console.log(err);
        this.toast.error('Something went wrong')
      })
    } else if (this.page_type == 'add') {
      this.producService.createProduct(this.formGroup.value).subscribe(res => {
        console.log(res);
        this.savebutton.nativeElement.disabled = false;
        this.isLoading = false;
        this.toast.success('product added successfully')
      }, err => {
        this.savebutton.nativeElement.disabled = false;
        this.isLoading = false;
        console.log(err);
        this.toast.error('Something went wrong')

      })
    }

    // this.formGroup.markAllAsTouched();
    // if (!this.formGroup.valid) {
    //   return;
    // }

    // const formValues = this.formGroup.value;
    // this.promocodeObj = Object.assign(this.promocodeObj, formValues);
    // if (this.id) {
    //   this.edit();
    // } else {
    //   this.create();
    // }
  }
  images: boolean = false;
  second_image: boolean = false;
  thir_image: boolean = false;

  images_File = null;
  second_image_File = null;
  third_image_File = null;
  getImage(file, id) {
    if (id == 'images') {
      this.images = true;

      this.images_File = file[0];
      this.formData.append('images', this.images_File);
      var reader = new FileReader();
      reader.readAsDataURL(file[0]);
      reader.onload = () => {
        (<HTMLElement>document.querySelector(`#${id}`)).setAttribute('src', <string>reader.result);
      }
    } else if (id == 'second_image') {
      this.second_image = true;

      this.second_image_File = file[0]
      this.formData.append('second_image', this.second_image_File);
      var reader = new FileReader();
      reader.readAsDataURL(file[0]);
      reader.onload = () => {
        (<HTMLElement>document.querySelector(`#${id}`)).setAttribute('src', <string>reader.result);
      }
    } else if (id == 'third_image') {
      this.thir_image == true;

      this.third_image_File = file[0]
      this.formData.append('third_image', this.third_image_File);
      var reader = new FileReader();
      reader.readAsDataURL(file[0]);
      reader.onload = () => {
        (<HTMLElement>document.querySelector(`#${id}`)).setAttribute('src', <string>reader.result);
      }
    }

  }




  create() {
    const sbCreate = this.productsService.create(this.promocodeObj).pipe(
      tap(() => this.router.navigate(['/ecommerce/products'])),
      catchError((errorMessage) => {
        console.error('UPDATE ERROR', errorMessage);
        return of(this.promocodeObj);
      })
    ).subscribe(res => {
      // this.promocodeObj = res as Product
    }
    );
    this.subscriptions.push(sbCreate);
  }

  changeTab(tabId: number) {
    this.activeTabId = tabId;
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sb => sb.unsubscribe());
  }

  // helpers for View
  isControlValid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation: string, controlName: string) {
    const control = this.formGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.dirty || control.touched;
  }
  back() {
    this.router.navigate(['/ecommerce/promo-code'])
  }
}

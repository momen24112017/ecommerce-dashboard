import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of, Subscription } from 'rxjs';
import { catchError, delay, finalize, tap } from 'rxjs/operators';
import { ProductService } from '../../../services/product.service';
import { ProductsService } from '../../../_services';

@Component({
  selector: 'app-delete-product-modal',
  templateUrl: './delete-product-modal.component.html',
  styleUrls: ['./delete-product-modal.component.scss']
})
export class DeleteProductModalComponent implements OnInit, OnDestroy {

  @Input() id: number;
  @Input() type: string;
  isLoading = false;
  subscriptions: Subscription[] = [];

  constructor(public modal: NgbActiveModal, private producservice: ProductService) { }

  ngOnInit(): void {
  }

  deleteProduct() {
    this.isLoading = true;
    if (this.type == 'product') {
      this.producservice.deleteProduct(this.id).subscribe(res => {
        console.log(res);
        this.isLoading = false;
        this.modal.close();
      }, err => {
        this.isLoading = false;
        this.modal.close();
      })
    }
    // const sb = this.productsService.delete(this.id).pipe(
    //   delay(1000), // Remove it from your code (just for showing loading)
    //   tap(() => this.modal.close()),
    //   catchError((err) => {
    //     this.modal.dismiss(err);
    //     return of(undefined);
    //   }),
    //   finalize(() => {
    //   })
    // ).subscribe();
    // this.subscriptions.push(sb);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sb => sb.unsubscribe());
  }
}

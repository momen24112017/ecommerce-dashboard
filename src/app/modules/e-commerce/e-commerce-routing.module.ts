import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ECommerceComponent } from './e-commerce.component';
import { CustomersComponent } from './customers/customers.component';
import { ProductsComponent } from './products/products.component';
import { ProductEditComponent } from './products/product-edit/product-edit.component';
import { BrandSettingComponent } from './brand-setting/brand-setting.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { PromocodeComponent } from './promocode/promocode.component';
import { ReviewsComponent } from './reviews/reviews.component';
import { EditFeeComponent } from './customers/components/edit-fee/edit-fee.component';
import { CategoryComponent } from './category/category.component';
import { EditCategoryComponent } from './category/edit-category/edit-category.component';
import { UsersComponent } from '../user-management/users/users.component';
import { OrdersComponent } from './orders/orders.component';
import { OrderDetailsComponent } from 'src/app/modules/e-commerce/orders/order-details/order-details.component';

const routes: Routes = [
  {
    path: '',
    component: ECommerceComponent,
    children: [
      {
        path: 'brand-setting',
        component: BrandSettingComponent,
      },
      {
        path: 'notification',
        component: NotificationsComponent,
      },
      {
        path: 'promocode',
        component: PromocodeComponent,
      },
      {
        path: 'reviews',
        component: ReviewsComponent,
      },



      {
        path: 'delivery-fees',
        component: CustomersComponent,
      },
      {
        path: 'products',
        component: ProductsComponent,
      },
      {
        path: 'users',
        component: UsersComponent,
      },
      {
        path: 'orders',
        component: OrdersComponent,
      },
      {
        path: 'product/add',
        component: ProductEditComponent
      },
      {
        path: 'product/edit',
        component: ProductEditComponent
      },
      {
        path: 'product/edit/:id',
        component: ProductEditComponent
      },
      {
        path: 'delivery-fee/edit/:id',
        component: EditFeeComponent
      },
      {
        path: 'delivery-fee/new-delivery-fee',
        component: EditFeeComponent
      },
      {
        path: 'categories',
        component: CategoryComponent
      },
      {
        path: 'orders/orderDetails',
        component: OrderDetailsComponent
      },
      {
        path: 'categories/edit/:id',
        component: EditCategoryComponent
      },
      {
        path: 'categories/add',
        component: EditCategoryComponent
      },
      { path: '', redirectTo: 'customers', pathMatch: 'full' },
      { path: '**', redirectTo: 'customers', pathMatch: 'full' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ECommerceRoutingModule {}

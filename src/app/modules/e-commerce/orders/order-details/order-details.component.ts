import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }

  orderDetails = {
    created_at: '',
    delivery_fees: '',
    id: '',
    order_code: '',
    order_status: '',
    promocode_id: '',
    subtotal_price: '',
    total_price: '',
    updated_at: '',
    user_id: {
      address: '',
      avatar: '',
      city: '',
      created_at: '',
      device_id: '',
      email: '',
      facebook_id: '',
      id: '',
      is_active: '',
      name: '',
      phone: '',
      role_id: '',
      settings: [],
      updated_at: ''
    }
  };
  user;
  ngOnInit(): void {
    this.route.queryParams.subscribe(res => {
      let data;
      data = res.data;
      JSON.parse(data)
      this.orderDetails = JSON.parse(data);
      this.user = res.res
      console.log(this.orderDetails);
      // console.log(JSON.parse(this.user));

    }, err => {
      console.log(err);

    })
  }

}

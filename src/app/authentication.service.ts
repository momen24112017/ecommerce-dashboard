import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  url="http://marouf.leatherworks.giraffecode.com/public/api"
  constructor(private http:HttpClient) { }
  login(email,password){
    return this.http.post('http://marouf.leatherworks.adminpanel.giraffecode.com/public/api/loginAdmin',{
      email:email,
      password:password
    })
  }
}
